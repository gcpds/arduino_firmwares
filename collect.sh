#!/usr/bin/bash

echo "Collecting libraries"
mkdir -p libraries
cp -r  ~/Arduino/libraries/OpenBCI_* libraries
ls -1 libraries

echo ""
echo "Collecting packages"
mkdir -p packages
cp -r  ~/.arduino15/packages/RFduino packages
cp -r  ~/.arduino15/packages/chipKIT packages
ls -1 packages

echo ""
echo "Collecting indexes"
mkdir -p indexes
cp  ~/.arduino15/package_rfduino_index.json indexes/
cp  ~/.arduino15/package_chipkit_index.json indexes/
ls -1 indexes
