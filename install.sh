#!/usr/bin/bash

echo "Installing libraries"
mkdir -p ~/Arduino/libraries
cp -r libraries/* ~/Arduino/libraries/
ls -1 ~/Arduino/libraries/

echo ""
echo "Installing packages"
mkdir -p ~/.arduino15/packages
cp -r packages/* ~/.arduino15/packages/
ls -1 ~/.arduino15/packages/

echo ""
echo "Installing indexes"
cp indexes/* ~/.arduino15/
ls -1 ~/.arduino15/*_index.json
